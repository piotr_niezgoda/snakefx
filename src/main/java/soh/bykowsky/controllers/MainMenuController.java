package soh.bykowsky.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import soh.bykowsky.SnakeFxApp;

public class MainMenuController {

    @FXML
    Button newGameButton;

    @FXML
    Button settingsButton;

    @FXML
    Button quitButton;

    @FXML
    public void startNewGame(ActionEvent event) {
        SnakeFxApp.gameInit.gameInit();
    }

    @FXML
    public void settings(ActionEvent event){
        SnakeFxApp.mainStage.setScene(SnakeFxApp.settingsMenuScene);
    }

    @FXML
    public void quit(ActionEvent event){
        SnakeFxApp.mainStage.close();
    }
}
