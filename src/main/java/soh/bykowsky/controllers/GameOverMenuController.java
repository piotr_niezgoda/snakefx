package soh.bykowsky.controllers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import soh.bykowsky.SnakeFxApp;

public class GameOverMenuController {

    @FXML
    Button newGameButton;

    @FXML
    Button mainMenuButton;

    @FXML
    Button quitButton;

    @FXML
    public void newGame(ActionEvent event) {
        SnakeFxApp.gameInit.gameLoop.setPaused(false);      // This shit fixes problem with game not closing thread after new game from pause menu
        SnakeFxApp.gameInit.gameLoop.setRunning(false);     // need to finish game loop to close the thread, because another gameInit.gameInit() creates another thread and the previous one stuck inside not finished game loop
        SnakeFxApp.gameOverMenuStage.close();
        SnakeFxApp.gameInit.gameInit();
    }

    @FXML
    public void mainMenu(ActionEvent event){
        SnakeFxApp.gameInit.gameLoop.setPaused(false);
        SnakeFxApp.gameInit.gameLoop.setRunning(false);
        SnakeFxApp.gameOverMenuStage.close();
        SnakeFxApp.mainStage.setScene(SnakeFxApp.mainMenuScene);
    }

    @FXML
    public void quit(ActionEvent event){
        SnakeFxApp.gameOverMenuStage.close();
        SnakeFxApp.gameInit.gameLoop.setPaused(false);
        SnakeFxApp.gameInit.gameLoop.setRunning(false);
        SnakeFxApp.mainStage.close();
    }
}
