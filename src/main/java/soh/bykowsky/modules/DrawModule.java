package soh.bykowsky.modules;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import soh.bykowsky.configurations.GameConfiguration;
import soh.bykowsky.gameObjects.Score;

public class DrawModule {
    static final Color BACKGROUND_COLOR = new Color(1.0, 1.0, 1.0,1.0);
    static final Color SNAKE_COLOR = new Color(0.0,0.0,0.0,1.0);
    static final Color FOOD_COLOR = new Color(1.0,0.0,0.0,1.0);

    private GraphicsContext graphicsContext;
    private GameConfiguration gc;
    private Score gameScore;

    public DrawModule(GraphicsContext graphicsContext, GameConfiguration gameConfiguration) {
        this.graphicsContext = graphicsContext;
        this.gc = gameConfiguration;
    }

    public DrawModule(GraphicsContext graphicsContext, GameConfiguration gc, Score score) {
        this.graphicsContext = graphicsContext;
        this.gc = gc;
        this.gameScore = score;
    }

    public void drawBackGround(){
        graphicsContext.setFill(BACKGROUND_COLOR);
        graphicsContext.fillRect(0,0,gc.getBoardWidth(),gc.getBoardHeight());
    }

    public void drawSnakePart(int headPosX, int headPosY){
        graphicsContext.setFill(SNAKE_COLOR);
        graphicsContext.fillRect(headPosX, headPosY, gc.getSnakePartSize(), gc.getSnakePartSize());
    }

    public void drawFood(int foodPosX, int foodPosY){
        graphicsContext.setFill(FOOD_COLOR);
        graphicsContext.fillRect(foodPosX, foodPosY, gc.getSnakePartSize(), gc.getSnakePartSize());
    }

    public void drawScore(){
        graphicsContext.strokeText("score: " + gameScore.getGameScore(), 20,20);
    }
}
