package soh.bykowsky;

import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.layout.Pane;
import soh.bykowsky.gameObjects.Food;
import soh.bykowsky.gameObjects.Score;
import soh.bykowsky.gameObjects.Snake;
import soh.bykowsky.modules.CollisionDetectionModule;
import soh.bykowsky.modules.DrawModule;
import soh.bykowsky.modules.GameStateUpdater;
import soh.bykowsky.modules.InputListener;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class GameInit {
    private GraphicsContext graphicsContext;
    public GameLoop gameLoop;

    public void gameInit(){
        Canvas canvas = new Canvas(SnakeFxApp.gameConfiguration.getBoardWidth(), SnakeFxApp.gameConfiguration.getBoardHeight());
        this.graphicsContext = canvas.getGraphicsContext2D();

        SnakeFxApp.gameScene = new Scene(new Pane(canvas));

        SnakeFxApp.mainStage.setOnCloseRequest(e -> {
            gameLoop.setPaused(false);
            gameLoop.setRunning(false);
        });
        SnakeFxApp.mainStage.setScene(SnakeFxApp.gameScene);


        // *************************************************************************************************
        Score gameScore = new Score();
        DrawModule drawModule = new DrawModule(graphicsContext, SnakeFxApp.gameConfiguration, gameScore);        // creating drawing module
        InputListener inputListener = new InputListener(SnakeFxApp.gameScene);         // creating InputListener module
        inputListener.getUserInput();                                   // inputListener starts to "listen" for key presses
        Snake snake = new Snake(SnakeFxApp.gameConfiguration);                     // creating snake
        Food food = new Food(SnakeFxApp.gameConfiguration, snake.getSnakeParts());                        // creating food
        GameStateUpdater gameStateUpdater = new GameStateUpdater(inputListener, snake, SnakeFxApp.gameConfiguration);     // creating gamestateUpdater module
        CollisionDetectionModule collisionDetectionModule = new CollisionDetectionModule(snake, food, SnakeFxApp.gameConfiguration);              // creating collisionDetectionModule module


        ScheduledExecutorService threadPool = Executors.newScheduledThreadPool(1);
        gameLoop = new GameLoop(drawModule, inputListener, gameStateUpdater, snake, food, SnakeFxApp.gameConfiguration, collisionDetectionModule, gameScore);   // game loop starts


        threadPool.schedule(gameLoop, 0, TimeUnit.SECONDS);
        threadPool.shutdown();
    }
}
