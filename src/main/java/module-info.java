module SnakeFx {
    requires javafx.fxml;
    requires javafx.controls;
    requires javafx.graphics;

    opens soh.bykowsky;
    opens soh.bykowsky.controllers;
}